<?php
include "../../../config/config.php";
$menu_id = 10;
$SessionUser = $SESSION->get_session_user();
($SessionUser['ses_uid']!='') ? $Session = $SessionUser : $Session = $SESSION->get_session(array('title'=>'GuestMenu', 'ses_name'=>'menu_without_login')); 
$USERAUTH->FrontEnd_check_akses_menu($menu_id, $Session);

// $get_data_filter = $RETRIEVE->retrieve_kontrak();
// pr($get_data_filter);
?>

<?php
	include"$path/meta.php";
	include"$path/header.php";
	include"$path/menu.php";
	
?>
	<!-- SQL Sementara -->
	<?php
		if($_SESSION['ses_satkerkode']){
			$cond = "AND kodeSatker ='".$_SESSION['ses_satkerkode']."' AND YEAR(TglPembukuan) = $TAHUN_AKTIF";
		}else{
			$cond = "AND YEAR(TglPembukuan) = $TAHUN_AKTIF";
		}
		/*print_r("SELECT Aset_ID,kodeSatker,kodeLokasi, kodeKelompok,TipeAset,TglPerolehan,TglPembukuan,MIN(noRegister) as minreg, MAX(noRegister) as maxreg,SUM(Kuantitas) as Kuantitas, SUM(NilaiPerolehan) as NilaiPerolehan FROM aset WHERE noKontrak is null  AND (StatusValidasi = 1 ) AND (Status_Validasi_Barang = 0 OR Status_Validasi_Barang IS NULL) 
			$cond
			GROUP BY kodeKelompok, kodeLokasi");*/
		$RKsql = mysql_query("SELECT Aset_ID,kodeSatker,kodeLokasi, kodeKelompok,TipeAset,TglPerolehan,TglPembukuan,MIN(noRegister) as minreg, MAX(noRegister) as maxreg,SUM(Kuantitas) as Kuantitas, SUM(NilaiPerolehan) as Total,NilaiPerolehan FROM aset WHERE noKontrak is null  AND (StatusValidasi =1) AND (Status_Validasi_Barang = 0 OR Status_Validasi_Barang IS NULL) 
			$cond
			GROUP BY kodeKelompok, kodeLokasi");
		while ($dataRKontrak = mysql_fetch_assoc($RKsql)){
			$rKontrak[] = $dataRKontrak;
		}
		
		if($rKontrak){
			foreach ($rKontrak as $key => $value) {
				$sqlnmBrg = mysql_query("SELECT Uraian FROM kelompok WHERE Kode = '{$value['kodeKelompok']}' LIMIT 1");
				while ($uraian = mysql_fetch_array($sqlnmBrg)){
						$tmp = $uraian;
						$rKontrak[$key]['uraian'] = $tmp['Uraian'];
					}
			}
		}

		if($rKontrak){
			foreach ($rKontrak as $keys => $values) {
				$sqlnmStkr = mysql_query("SELECT NamaSatker FROM satker WHERE Kode = '".$values['kodeSatker']."' AND Kd_Ruang IS NULL LIMIT 1 ");
				while ($namaSatker = mysql_fetch_array($sqlnmStkr)){
						$tmps = $namaSatker;
						$rKontrak[$keys]['skpd'] = $tmps['NamaSatker'];
					}
			}
		}
			

	?>
	<section id="main">
		<ul class="breadcrumb">
			  <li><a href="#"><i class="fa fa-home fa-2x"></i>  Home</a> <span class="divider"><b>&raquo;</b></span></li>
			  <li><a href="#">Inventarisasi</a><span class="divider"><b>&raquo;</b></span></li>
			  <li class="active">Entri Inventarisasi</li>
			  <?php SignInOut();?>
			</ul>
			<div class="breadcrumb">
				<div class="title">Inventarisasi</div>
				<div class="subtitle">Entri Inventarisasi</div>
			</div>

			<div class="grey-container shortcut-wrapper">
				<a class="shortcut-link" href="<?=$url_rewrite?>/module/inventarisasi/entri/entri_hasil_inventarisasi.php">
					<span class="fa-stack fa-lg">
				      <i class="fa fa-circle fa-stack-2x"></i>
				      <i class="fa fa-inverse fa-stack-1x">1</i>
				    </span>
					<span class="text">Entri Inventarisasi</span>
				</a>
				<a class="shortcut-link active" href="<?=$url_rewrite?>/module/inventarisasi/entri/rincian_hasil_inventarisasi.php">
					<span class="fa-stack fa-lg">
				      <i class="fa fa-circle fa-stack-2x"></i>
				      <i class="fa fa-inverse fa-stack-1x">2</i>
				    </span>
					<span class="text">Rincian Inventarisasi</span>
				</a>
			</div>

		<section class="formLegend">
			<div id="demo">
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Satker</th>
						<th>Kode Barang</th>
						<th>Nama Barang</th>
						<th>No Register</th>
						<th>Tgl.Perolehan</th>
						<th>Tgl.Pembukuan</th>
						<th>Jumlah</th>
						<th>Nilai Perolehan</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
				<?php
					if($rKontrak){
						$i = 1;
						foreach ($rKontrak as $key => $value) {
							
						
				?>
					<tr class="gradeA">
						<td><?=$i?></td>
						<td><?=$value['skpd']?> [<?=$value['kodeSatker']?>]</td>
						<td class="center"><?=$value['kodeKelompok']?></td>
						<td class="center"><?=$value['uraian']?></td>
						<td class="center"><?=$value['minreg']?> s/d <?=$value['maxreg']?></td>
						<td class="center"><?=$value['TglPerolehan']?></td>
						<td class="center"><?=$value['TglPembukuan']?></td>
						<td class="center"><?=number_format($value['Kuantitas'])?></td>
						<td class="center"><?=number_format($value['NilaiPerolehan'])?></td>
						<td class="center"><?=number_format($value['Total'])?></td>
					</tr>
				<?php
						$i++;
						}
					}	
				?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="6">&nbsp;</th>
					</tr>
				</tfoot>
			</table>
			</div>	    
		</section> 
		     
	</section>
	
<?php
	include"$path/footer.php";
?>

