<?php

include "../../../config/config.php"; 
/*start background process
ket : param
$argv[1] = skpd
$argv[2] = tipe Aset (B,C,E)
*/
if (!is_dir('../../../log/migrasi')) {
    mkdir('../../../log/migrasi', 0777, true);
}

$link = mysqli_connect($CONFIG['default']['db_host'],$CONFIG['default']['db_user'],$CONFIG['default']['db_pass'],$CONFIG['default']['db_name']) or die("Error " . mysqli_error($link)); 


//list all satker
$qsat = "SELECT kode,NamaSatker FROM satker where kode is not null and KodeUnit is not null and Gudang is not null and Kd_Ruang is NULL and kode = '04.02.01.01'";
$rsat = $link->query($qsat) or die("Error in the consult.." . mysqli_error($link)); 
while($dtrsat = mysqli_fetch_assoc($rsat)){
	if($dtrsat != ''){
		$satker[] = $dtrsat['kode'];
	}	
}

//list satker yg ada datanya
$qsatcmpr = "SELECT kodeSatker FROM aset where Status_Validasi_Barang = 1 GROUP BY kodeSatker";
$rsatcmpr = $link->query($qsatcmpr) or die("Error in the consult.." . mysqli_error($link)); 
while($dtrsatcmpr = mysqli_fetch_assoc($rsatcmpr)){
	if($dtrsatcmpr != ''){
		$satkercmpr[] = $dtrsatcmpr['kodeSatker'];
	}	
}
//list array skpd fix
$result=array_intersect($satkercmpr,$satker);

$tipeAset = array("0"=>'B',"1"=>'C',"2"=>'E');
foreach ($tipeAset as $tipe) {
	
	foreach ($result as $skpd) {

		$log = $skpd.'-'.$tipe;
		$status=exec("php migrasi-kebijakan-akuntansi.php $skpd $tipe  > ../../../log/migrasi/$log.txt 2>&1 &");
		sleep(5);
	
	}    
}
?>
