<?php
include "../../../config/database.php";

$link = mysqli_connect($CONFIG['default']['db_host'],$CONFIG['default']['db_user'],$CONFIG['default']['db_pass'],$CONFIG['default']['db_name']) or die("Error " . mysqli_error($link)); 

//start process
//$time_start = microtime(true); 

$skpd = $argv[1];
$tipeAset = $argv[2];


$QueryMesin_1="create temporary table mesin_ori as select a.Mesin_ID,a.Aset_ID, a.kodeKelompok, a.kodeSatker, a.kodeLokasi, a.noRegister, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan, a.Status_Validasi_Barang, a.StatusTampil, a.Tahun, a.NilaiPerolehan, a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.Merk, a.Model, a.Ukuran, a.Silinder, a.MerkMesin, a.JumlahMesin,a.Material, a.NoSeri, a.NoRangka, a.NoMesin, a.NoSTNK, a.TglSTNK, a.NoBPKB, a.TglBPKB, a.NoDokumen, a.TglDokumen, a.Pabrik, a.TahunBuat, a.BahanBakar, a.NegaraAsal, a.NegaraRakit, a.Kapasitas, a.Bobot, a.GUID, a.MasaManfaat, a.AkumulasiPenyusutan, a.NilaiBuku, a.PenyusutanPerTahun,a.TahunPenyusutan from mesin a where a.TglPerolehan <='2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.kodeSatker = '$skpd'";

$QueryMesin_2="ALTER table mesin_ori add primary key(Mesin_ID)";  	

$QueryMesin_3="replace into mesin_ori (Mesin_ID, Aset_ID, kodeKelompok, kodeSatker, kodeLokasi, noRegister, TglPerolehan, TglPembukuan, kodeData, kodeKA, kodeRuangan, Status_Validasi_Barang, StatusTampil, Tahun, NilaiPerolehan, Alamat, Info, AsalUsul, kondisi, CaraPerolehan, Merk, Model, Ukuran, Silinder, MerkMesin, JumlahMesin, Material, NoSeri, NoRangka, NoMesin, NoSTNK, TglSTNK, NoBPKB, TglBPKB, NoDokumen, TglDokumen, Pabrik, TahunBuat, BahanBakar, NegaraAsal, NegaraRakit, Kapasitas, Bobot, GUID, MasaManfaat, AkumulasiPenyusutan, NilaiBuku, PenyusutanPerTahun,TahunPenyusutan) select a.Mesin_ID, a.Aset_ID, a.kodeKelompok, a.kodeSatker, a.kodeLokasi, a.noRegister, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan, a.Status_Validasi_Barang, a.StatusTampil, a.Tahun, if(a.NilaiPerolehan_Awal!=0,a.NilaiPerolehan_Awal,a.NilaiPerolehan), a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.Merk, a.Model, a.Ukuran, a.Silinder, a.MerkMesin, a.JumlahMesin,a.Material, a.NoSeri, a.NoRangka, a.NoMesin, a.NoSTNK, a.TglSTNK, a.NoBPKB, a.TglBPKB, a.NoDokumen, a.TglDokumen, a.Pabrik, a.TahunBuat, a.BahanBakar, a.NegaraAsal, a.NegaraRakit, a.Kapasitas, a.Bobot, a.GUID, a.MasaManfaat, if(a.AkumulasiPenyusutan_Awal is not null and a.AkumulasiPenyusutan_Awal !=0 and a.Kd_Riwayat in(7,21,281,51,55),a.AkumulasiPenyusutan_Awal,a.AkumulasiPenyusutan), if(a.NilaiBuku_Awal is not null and (a.NilaiPerolehan_Awal=a.AkumulasiPenyusutan_Awal or a.NilaiBuku_Awal!=0 or a.Kd_Riwayat in(2)) ,a.NilaiBuku_Awal,a.NilaiBuku), if(a.PenyusutanPerTahun_Awal is not null and a.PenyusutanPerTahun_Awal !=0,a.PenyusutanPerTahun_Awal,a.PenyusutanPerTahun),a.TahunPenyusutan from log_mesin a inner join mesin t on t.Aset_ID=a.Aset_ID inner join mesin t_2 on t_2.Aset_ID=t.Aset_ID and t.Aset_ID is not null and t.Aset_ID != 0 where a.TglPerolehan <='2018-12-31' AND a.TglPerubahan >'2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.kodeSatker = '$skpd' AND (a.Kd_Riwayat != '77' AND a.Kd_Riwayat != '0') order by a.log_id desc,a.TglPerubahan desc";  

$QueryMesin_4="replace into mesin_ori (Mesin_ID, Aset_ID, kodeKelompok, kodeSatker, kodeLokasi, noRegister, TglPerolehan, TglPembukuan, kodeData, kodeKA, kodeRuangan, Status_Validasi_Barang, StatusTampil, Tahun, NilaiPerolehan, Alamat, Info, AsalUsul, kondisi, CaraPerolehan, Merk, Model, Ukuran, Silinder, MerkMesin, JumlahMesin, Material, NoSeri, NoRangka, NoMesin, NoSTNK, TglSTNK, NoBPKB, TglBPKB, NoDokumen, TglDokumen, Pabrik, TahunBuat, BahanBakar, NegaraAsal, NegaraRakit, Kapasitas, Bobot, GUID, MasaManfaat, AkumulasiPenyusutan, NilaiBuku, PenyusutanPerTahun, TahunPenyusutan) select a.Mesin_ID, a.Aset_ID, a.kodeKelompok, a.SatkerAwal, concat(left(a.kodeLokasi,9),left(a.SatkerAwal,6),lpad(right(a.Tahun,2),2,'0'),'.',right(a.SatkerAwal,5)), a.NomorRegAwal, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan, a.Status_Validasi_Barang, a.StatusTampil, a.Tahun, a.NilaiPerolehan, a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.Merk, a.Model, a.Ukuran, a.Silinder, a.MerkMesin, a.JumlahMesin, a.Material, a.NoSeri, a.NoRangka, a.NoMesin, a.NoSTNK, a.TglSTNK, a.NoBPKB, a.TglBPKB, a.NoDokumen, a.TglDokumen, a.Pabrik, a.TahunBuat, a.BahanBakar, a.NegaraAsal, a.NegaraRakit, a.Kapasitas, a.Bobot, a.GUID, a.MasaManfaat, a.AkumulasiPenyusutan, a.NilaiBuku, a.PenyusutanPerTahun,t.TahunPenyusutan from view_mutasi_mesin a inner join mesin t on t.Aset_ID=a.Aset_ID inner join mesin t_2 on t_2.Aset_ID=t.Aset_ID and t.Aset_ID is not null and t.Aset_ID != 0 where a.TglPerolehan <='2018-12-31' AND a.TglSKKDH >'2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.SatkerTujuan = '$skpd' order by a.TglSKKDH desc";

$QueryBangunan_1="create temporary table bangunan_ori as select a.Bangunan_ID, a.Aset_ID, a.kodeKelompok, a.kodeSatker, a.kodeLokasi, a.noRegister, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan, a.Status_Validasi_Barang, a.StatusTampil, a.Tahun, a.NilaiPerolehan, a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.TglPakai, a.Konstruksi, a.Beton, a.JumlahLantai, a.LuasLantai, a.Dinding, a.Lantai, a.LangitLangit, a.Atap, a.NoSurat, a.TglSurat, a.NoIMB, a.TglIMB, a.StatusTanah, a.NoSertifikat, a.TglSertifikat, a.Tanah_ID, a.Tmp_Tingkat, a.Tmp_Beton, a.Tmp_Luas, a.KelompokTanah_ID, a.GUID, a.TglPembangunan, a.MasaManfaat, a.AkumulasiPenyusutan, a.NilaiBuku, a.PenyusutanPerTahun,a.TahunPenyusutan from bangunan a where a.TglPerolehan <='2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.kodeSatker = '$skpd'";

$QueryBangunan_2="ALTER table bangunan_ori add primary key(Bangunan_ID)";

$QueryBangunan_3="replace into bangunan_ori (Bangunan_ID, Aset_ID, kodeKelompok, kodeSatker, kodeLokasi, noRegister, TglPerolehan, TglPembukuan, kodeData, kodeKA, kodeRuangan, Status_Validasi_Barang, StatusTampil, Tahun, NilaiPerolehan, Alamat, Info, AsalUsul, kondisi, CaraPerolehan, TglPakai, Konstruksi, Beton, JumlahLantai, LuasLantai, Dinding, Lantai, LangitLangit, Atap, NoSurat, TglSurat, NoIMB, TglIMB, StatusTanah, NoSertifikat, TglSertifikat, Tanah_ID, Tmp_Tingkat, Tmp_Beton, Tmp_Luas, KelompokTanah_ID, GUID, TglPembangunan, MasaManfaat, AkumulasiPenyusutan, NilaiBuku, PenyusutanPerTahun,TahunPenyusutan) select a.Bangunan_ID, a.Aset_ID, a.kodeKelompok, a.kodeSatker, a.kodeLokasi, a.noRegister, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan, a.Status_Validasi_Barang, a.StatusTampil, a.Tahun, if(a.NilaiPerolehan_Awal!=0,a.NilaiPerolehan_Awal,a.NilaiPerolehan), a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.TglPakai, a.Konstruksi, a.Beton, a.JumlahLantai, a.LuasLantai, a.Dinding, a.Lantai, a.LangitLangit, a.Atap, a.NoSurat, a.TglSurat, a.NoIMB, a.TglIMB, a.StatusTanah, a.NoSertifikat, a.TglSertifikat, a.Tanah_ID, a.Tmp_Tingkat, a.Tmp_Beton, a.Tmp_Luas, a.KelompokTanah_ID, a.GUID, a.TglPembangunan, a.MasaManfaat, if(a.AkumulasiPenyusutan_Awal is not null and a.AkumulasiPenyusutan_Awal !=0 and a.Kd_Riwayat in(7,21,55,281,51),a.AkumulasiPenyusutan_Awal,a.AkumulasiPenyusutan), if(a.NilaiBuku_Awal is not null and (a.NilaiPerolehan_Awal=a.AkumulasiPenyusutan_Awal or a.NilaiBuku_Awal!=0 or a.Kd_Riwayat in(2)) ,a.NilaiBuku_Awal,a.NilaiBuku), if(a.PenyusutanPerTahun_Awal is not null and a.PenyusutanPerTahun_Awal !=0,a.PenyusutanPerTahun_Awal,a.PenyusutanPerTahun),a.TahunPenyusutan from log_bangunan a inner join bangunan t on t.Aset_ID=a.Aset_ID inner join bangunan t_2 on t_2.Aset_ID=t.Aset_ID and t.Aset_ID is not null and t.Aset_ID != 0 where a.TglPerolehan <='2018-12-31' AND a.TglPerubahan >'2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.kodeSatker = '$skpd' AND (a.Kd_Riwayat != '77' AND a.Kd_Riwayat != '0') order by a.log_id desc,a.TglPerubahan desc";

$QueryBangunan_4="replace into bangunan_ori (Bangunan_ID, Aset_ID, kodeKelompok, kodeSatker, kodeLokasi, noRegister, TglPerolehan, TglPembukuan, kodeData, kodeKA, kodeRuangan, Status_Validasi_Barang, StatusTampil, Tahun, NilaiPerolehan, Alamat, Info, AsalUsul, kondisi, CaraPerolehan, TglPakai, Konstruksi, Beton, JumlahLantai, LuasLantai, Dinding, Lantai, LangitLangit, Atap, NoSurat, TglSurat, NoIMB, TglIMB, StatusTanah, NoSertifikat, TglSertifikat, Tanah_ID, Tmp_Tingkat, Tmp_Beton, Tmp_Luas, KelompokTanah_ID, GUID, TglPembangunan, MasaManfaat, AkumulasiPenyusutan, NilaiBuku, PenyusutanPerTahun,TahunPenyusutan) select a.Bangunan_ID, a.Aset_ID, a.kodeKelompok, a.SatkerAwal,concat(left(a.kodeLokasi,9),left(a.SatkerAwal,6),lpad(right(a.Tahun,2),2,'0'),'.',right(a.SatkerAwal,5)), a.NomorRegAwal, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan,a.Status_Validasi_Barang, a.StatusTampil, a.Tahun, a.NilaiPerolehan, a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.TglPakai, a.Konstruksi, a.Beton, a.JumlahLantai, a.LuasLantai, a.Dinding, a.Lantai, a.LangitLangit, a.Atap, a.NoSurat, a.TglSurat, a.NoIMB, a.TglIMB, a.StatusTanah, a.NoSertifikat, a.TglSertifikat, a.Tanah_ID, a.Tmp_Tingkat, a.Tmp_Beton, a.Tmp_Luas, a.KelompokTanah_ID, a.GUID, a.TglPembangunan, a.MasaManfaat, a.AkumulasiPenyusutan, a.NilaiBuku, a.PenyusutanPerTahun,t.TahunPenyusutan from view_mutasi_bangunan a inner join bangunan t on t.Aset_ID=a.Aset_ID inner join bangunan t_2 on t_2.Aset_ID=t.Aset_ID and t.Aset_ID is not null and t.Aset_ID != 0 where a.TglPerolehan <='2018-12-31' AND a.TglSKKDH >'2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.SatkerTujuan = '$skpd' order by a.TglSKKDH desc";

$QueryAsetLain_1 ="create temporary table asetlain_ori as select a.AsetLain_ID, a.Aset_ID, a.kodeKelompok, a.kodeSatker, a.kodeLokasi, a.noRegister, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan,a.Status_Validasi_Barang, a.StatusTampil,a.Tahun, a.NilaiPerolehan, a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.Judul, a.AsalDaerah, a.Pengarang, a.Penerbit, a.Spesifikasi, a.TahunTerbit, a.ISBN, a.Material, a.Ukuran, a.GUID, a.MasaManfaat, a.AkumulasiPenyusutan, a.NilaiBuku, a.PenyusutanPerTahun from asetlain a where a.TglPerolehan <='2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.kodeSatker = '$skpd'";

$QueryAsetLain_2 ="ALTER table asetlain_ori add primary key(AsetLain_ID)";

$QueryAsetLain_3 ="replace into asetlain_ori (AsetLain_ID, Aset_ID, kodeKelompok, kodeSatker, kodeLokasi, noRegister, TglPerolehan, TglPembukuan, kodeData, kodeKA, kodeRuangan, Status_Validasi_Barang, StatusTampil, Tahun, NilaiPerolehan, Alamat, Info, AsalUsul, kondisi, CaraPerolehan, Judul, AsalDaerah, Pengarang, Penerbit, Spesifikasi, TahunTerbit, ISBN, Material, Ukuran, GUID, MasaManfaat, AkumulasiPenyusutan, NilaiBuku, PenyusutanPerTahun) select a.AsetLain_ID, a.Aset_ID, a.kodeKelompok, a.kodeSatker, a.kodeLokasi, a.noRegister, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan,a.Status_Validasi_Barang, a.StatusTampil,a.Tahun, if(a.NilaiPerolehan_Awal!=0,a.NilaiPerolehan_Awal,a.NilaiPerolehan), a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.Judul, a.AsalDaerah, a.Pengarang, a.Penerbit, a.Spesifikasi, a.TahunTerbit, a.ISBN, a.Material, a.Ukuran, a.GUID, a.MasaManfaat, a.AkumulasiPenyusutan, a.NilaiBuku, a.PenyusutanPerTahun from log_asetlain a inner join asetlain t on t.Aset_ID=a.Aset_ID inner join asetlain t_2 on t_2.Aset_ID=t.Aset_ID and t.Aset_ID is not null and t.Aset_ID != 0 where a.TglPerolehan <='2018-12-31' AND a.TglPerubahan >'2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.kodeSatker = '$skpd' AND (a.Kd_Riwayat != '77' AND a.Kd_Riwayat != '0') order by a.log_id desc,a.TglPerubahan desc";

$QueryAsetLain_4 ="replace into asetlain_ori (AsetLain_ID, Aset_ID, kodeKelompok, kodeSatker, kodeLokasi, noRegister, TglPerolehan, TglPembukuan, kodeData, kodeKA, kodeRuangan, Status_Validasi_Barang, StatusTampil, Tahun, NilaiPerolehan, Alamat, Info, AsalUsul, kondisi, CaraPerolehan, Judul, AsalDaerah, Pengarang, Penerbit, Spesifikasi, TahunTerbit, ISBN, Material, Ukuran, GUID, MasaManfaat, AkumulasiPenyusutan, NilaiBuku, PenyusutanPerTahun) select a.AsetLain_ID, a.Aset_ID, a.kodeKelompok, SatkerAwal,concat(left(a.kodeLokasi,9),left(a.SatkerAwal,6),lpad(right(a.Tahun,2),2,'0'),'.',right(a.SatkerAwal,5)), a.NomorRegAwal, a.TglPerolehan, a.TglPembukuan, a.kodeData, a.kodeKA, a.kodeRuangan,a.Status_Validasi_Barang, a.StatusTampil,a.Tahun, a.NilaiPerolehan, a.Alamat, a.Info, a.AsalUsul, a.kondisi, a.CaraPerolehan, a.Judul, a.AsalDaerah, a.Pengarang, a.Penerbit, a.Spesifikasi, a.TahunTerbit, a.ISBN, a.Material, a.Ukuran, a.GUID, a.MasaManfaat, a.AkumulasiPenyusutan, a.NilaiBuku, a.PenyusutanPerTahun from view_mutasi_asetlain a inner join asetlain t on t.Aset_ID=a.Aset_ID inner join asetlain t_2 on t_2.Aset_ID=t.Aset_ID and t.Aset_ID is not null and t.Aset_ID != 0 where a.TglPerolehan <='2018-12-31' AND a.TglSKKDH >'2018-12-31' AND a.TglPembukuan <='2018-12-31' AND a.SatkerTujuan = '$skpd' order by a.TglSKKDH desc";

if($tipeAset == 'B'){
	$AllTableTemp = array($QueryMesin_1,$QueryMesin_2,$QueryMesin_3,$QueryMesin_4);
}elseif($tipeAset == 'C'){
	$AllTableTemp = array($QueryBangunan_1,$QueryBangunan_2,$QueryBangunan_3,$QueryBangunan_4);
}elseif($tipeAset == 'E'){
	$AllTableTemp = array($QueryAsetLain_1,$QueryAsetLain_2,$QueryAsetLain_3,$QueryAsetLain_4);
}

for ($i = 0; $i < count($AllTableTemp); $i++){
	/*echo "query_$i =".$AllTableTemp[$i];
	echo "<br>";
	echo "<br>";*/
	// exit;
	$resultQuery = $link->query($AllTableTemp[$i]) or die("Error in the consult.." . mysqli_error($link)); 
}

if($tipeAset == 'B'){
	$sqlData = "SELECT * FROM mesin_ori WHERE kodeSatker = '$skpd' 
				AND Status_Validasi_Barang = 1 AND StatusTampil = 1 
				AND kondisi != 3 AND kondisi != 4 AND kondisi != 5
				AND kodeLokasi like '12%'";

	$res = $link->query($sqlData) or die("Error in the consult.." . mysqli_error($link)); 
	while($data = mysqli_fetch_assoc($res)) {

		echo "UPDATE aset SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
		echo "UPDATE mesin SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
		echo "UPDATE log_mesin SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
			
	}
}elseif($tipeAset == 'C'){
	$sqlData = "SELECT * FROM bangunan_ori WHERE kodeSatker = '$skpd' 
				AND Status_Validasi_Barang = 1 AND StatusTampil = 1
				AND kondisi != 3 AND kondisi != 4 AND kondisi != 5
				AND kodeLokasi like '12%'";

	$res = $link->query($sqlData) or die("Error in the consult.." . mysqli_error($link)); 
	while($data = mysqli_fetch_assoc($res)) {

		echo "UPDATE aset SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
		echo "UPDATE bangunan SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
		echo "UPDATE log_bangunan SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
			
	}
}elseif($tipeAset == 'E'){
	$sqlData = "SELECT * FROM asetlain_ori WHERE kodeSatker = '$skpd' 
				AND Status_Validasi_Barang = 1 AND StatusTampil = 1
				AND kondisi != 3 AND kondisi != 4 AND kondisi != 5
				AND kodeLokasi like '12%'";

	$res = $link->query($sqlData) or die("Error in the consult.." . mysqli_error($link)); 
	while($data = mysqli_fetch_assoc($res)) {

		echo "UPDATE aset SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
		echo "UPDATE asetlain SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
		echo "UPDATE log_asetlain SET kodeKA = 1 WHERE Aset_ID = ".$data['Aset_ID'].";"."\n\n";
			
	}
}
//$time_end = microtime(true);

//dividing with 60 will give the execution time in minutes other wise seconds
//$execution_time = ($time_end - $time_start)/60;

//execution time of the script
//echo '<b>Total Execution Time:</b> '.$execution_time.' Mins\n\n';

//echo "=================== Process Complete. Thank you ===================\n\n";

?>
